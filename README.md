# AP CS

Contains the files we use in our sessions. Should be updated right after every
session.

File Structure:

```
|- firstsessions (Session 1, Session 2)
|-- HelloWorld.java
|-- UsingScanner.java
|- session3 (Objects, Classes, Methods)
|-- Dog.java
|-- MyDogs.java