package session9;

import java.util.Scanner;

public class IterationAgain {

    public static void main(String[] args) {
        whileLoops();
    }

    public static void recapForLoops() {
        // print all even numbers that are multiples of 5 from 1 to 100 (inclusive)
        /*
            10
            20
            ...
            100
         */
        // Your solution
        for (int i = 10; i <= 100; i += 10) {
            System.out.println(i);
        }

        // My preferred solution
        for (int i = 5; i < 100; i += 5) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }

        // print all odd numbers that are multiples of 3 from 1 to 100 in reverse order (1 and 100 inclusive)
        /*
            99
            93
            ....
            3
         */
        for (int i = 99; i >= 1; i -= 3) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }

        // Just another solution
        for (int i = 100; i >= 1; i--) {
            if (i % 3 == 0 && i % 2 == 1) {
                System.out.println(i);
            }
        }
    }

    public static void nestedLoops() {
        /*
        print the following
            0
            01
            012
            0123
            012345
            0123456
            01234567
            012345678
            0123456789
         */

        /*
            Loop 0 : prints all numbers from 0 to 0. Then prints a new line
            Loop 1 : prints all numbers from 0 to 1. Then prints a new line
            Loop 2 : prints all numbers from 0 to 2. Then prints a new line
            ...
            Loop 9 : prints all numbers from 0 to 9. Then prints a new line

            Loop n : prints all numbers from 0 to n (n inclusive). Then prints a new line
         */

        /*
            for each number n from 0 to 9,
                print all numbers from 0 to n (n inclusive). Then prints a new line
         */

        for (int n = 0; n <= 9; n++) {      // for each number n from 0 to 9

            for (int i = 0; i <= n; i++) {      // print all numbers from 0 to n (n inclusive)
                System.out.print(i);
            }
            System.out.println();       // print a new line

        }

        /*
            n = 0; true
                i = 0; true, print 0
                i = 1; false
                print a new line (outside the inner loop)
            n = 1; true
                i = 0; true; print 0
                i = 1; true; print 1
                i = 2; false
                print a new line (outside the inner loop)
            n = 2; true
                i = 0; true; print 0
                i = 1; true print 1
                i = 2; true; print 2
                i = 3; false
                print a new line (outside the inner loop)
            ...
            n = 9; true
                i = 0; true; print 0
                ...
                i = 9; true; print 9
                i = 10; false
                print a new line (outside the inner loop)
            n = 10; false
         */

    }

    public static void nestedLoopsWithoutNumber() {
        /*
            *
            ***
            *****
            *******
            *********
         */

        /* INNER LOOP:
            loop 0 : prints * 1 time
            loop 1 : prints * 3 times
            loop 2 : prints * 5 times
            loop 3 : prints * 7 times
            loop 4 : prints * 9 times

            loop n : prints * (2n + 1) times

            Can also do it as:

            loop 1 : prints * 1 time
            loop 2 : prints * 3 times
            loop 3 : prints * 5 times
            loop 4 : prints * 7 times
            loop 5 : prints * 9 times

            loop n : prints * (2n - 1) times

         */

        /*
            for n from 0 to 5 (not inclusive)
                print * (2*n + 1) times
                print a new line
         */

        for (int n = 0; n < 5; n++) {
            for (int i = 0; i < (2 * n + 1); i++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int n = 0; n < 10; n++) {
            if (n % 2 == 1) {
                // n = 1, 3, 5, 7, 9
                for (int i = 0; i < n; i++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }

        /*
            n = 0; true
                n % 2 == 1 -> false
            n = 1; true
                n % 2 == 1 -> true
                    i = 0; true, print *
                    i = 1; false
                    print a new line
            n = 2; true
                n % 2 == 1 -> false
            n = 3; true
                n % 2 == 1 -> true
                    i = 0; true, print *
                    i = 1; true; print *
                    i = 2; true; print *
                    i = 3; false
                    print a new line
            ...
            n = 9; true
                n % 2 == 1 -> true
                    i = 0; true, print *
                    ...
                    i = 9; true, print *
                    i = 10; false
                    print a new line
            n = 10; false
         */
    }

    public static void howToCalculateTheSpecialFormula() {
        /*
               n   |   other value
            _______|________________
               0   |   1
               1   |   3
               2   |   5
               3   |   7
               4   |   9

           2n +/- ?     -> 2 is because the other value increments by 2 while n increments by 1
           2n + 1       -> +1 is because we can test the formulate with n = 0 or n = 1 (or any value of n)


               n   |   other value
            _______|________________
               1   |   1
               2   |   3
               3   |   5
               4   |   7
               5   |   9

           2n - 1


               n   |   other value
            _______|________________
               0   |   1
               1   |   7
               2   |   13
               3   |   19

               6n + 1       // if the first n is 0, then it's almost always + the value at 0




               n   |   other value
            _______|________________
               1   |   1
               2   |   8
               3   |   15
               4   |   22

               7n - 6
         */
    }

    public static void whileLoops() {
        // Ask the user for all their favorite numbers and then give them the average of their favorite numbers
        System.out.println("What are all your favorite integer numbers? Say stop once you're done");
        Scanner scan = new Scanner(System.in);

//        while (THIS CONDITION IS TRUE) {
//            DO THIS
//        }

        // average = sum of all numbers / count of numbers
        int sumOfNumbers = 0;
        int countOfNumber = 0;

        while (scan.hasNextInt()) {
            int nextNumber = scan.nextInt();
            sumOfNumbers += nextNumber;
            countOfNumber++;
        }

        System.out.println("The average of your favorite number is " + sumOfNumbers * 1.0 / countOfNumber);


        // If you're ever confused about the condition for a while loop. Think of when you want it to stop, then negate that

        // in this example: stop when scanner no longer has a new/next int.
        // So the condition: continue while the scanner has a next int
    }
}
