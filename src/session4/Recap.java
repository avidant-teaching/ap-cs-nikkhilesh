package session4;

public class Recap {

    public static void main(String[] args) {
        String name = "Nik";

        // the real object oriented way. No need to use it in general
        String myName = new String("Nik");
    }

    public void multiLineString() {
        String welcomeMessage = "Hello! Welcome to my brand new video game where I like to play minesweeper " +
                "and battle zombies all at the same time. Hope you enjoy it. If you don't, you can file a bug at my " +
                "website or contact me";

    }

    public void usingNumberAsString() {
        String name = "12";

//Brackets, Division/Multiplication, Addition/Subtraction

        String otherString1 = 4 + "12"; // 412
        String otherString2 = 4 + name; // 412
        String otherString3 = "12" + 4; // 124
        String otherString4 = 4 + "12" + 3; // 4123
        String otherString5 = 4 + 3 + "12"; // 712
        String otherString6 = "12" + 4 + 3; // 1243
        String otherString7 = "12" + 4 / 3; // 121
//        String otherString8 = 4 / "12" + 3; // Won't compile
    }

    public void misc() {
        String name = "Nik";

        String otherString1 = 4 + "Nik"; // 4Nik
        String otherString2 = 4 + name; // 4Nik
        String otherString3 = "Nik" + 4; // Nik4
        String otherString4 = 4 + "Nik" + 3; // 3Nik3
        String otherString5 = 4 + 3 + "Nik"; // 7Nik
        String otherString6 = "Nik" + 4 + 3; // Nik7
        String otherString7 = "Nik" + 4 / 3; // Nik1
    }

}
