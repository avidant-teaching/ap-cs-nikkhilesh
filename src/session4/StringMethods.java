package session4;

public class StringMethods {

    public static void main(String[] args) {
//        length();
//            indexOf();
//        substring();
//        misc();

        compareTo();
    }

    public static void length() {
        String name = "Nik";
        // What would length do: give the length of the string. Eg: Nik -> 3

        System.out.println(name.length());  // 3
        System.out.println("".length());    // 0
        System.out.println("Random".length());  // 6
        System.out.println("A".length());   // 1

        String newName = "123" + name;
        System.out.println(newName.length());    // 6
    }

    public static void substring() {
        // What is a string -> class
        // What does a string class represent -> a set of characters in a specific order
        // substring -> part of a string

        // String -> abcdef, substrings -> {"", "a", "ab", "abc", .., "b", "bc"..."abcdef"}
        // substring method is used to get part of a string from position a to position b

        // String -> abcdef, substring (0 to 3) -> abc [first index is included, the second is excluded]
        //                   substring (1 to 4) -> bcd

        String name = "Avidant";

        // substring from 0 to 3 for name
        // method -> substring
        // input 1, starting index
        // input 2, ending index (excluded)
        System.out.println(name.substring(0, 3));   // Avi

        // Substring -> only start position, no end position
        System.out.println(name.substring(3));  // dant

    }

    public static void indexOf() {
        // index -> table of values
        // string -> position of each character

        // string ->    abc
        // index ->     012

        String name = "Nik";
        // index 0 -> N
        // index 1 -> i
        // index 2 -> k

        // String N and a char N
        // Strings -> " and char -> '
        char character = 'N';
        System.out.println(name.indexOf(character));    // 0

        // missing is represented by a -1
        System.out.println(name.indexOf('A'));  //  -1
        System.out.println(name.indexOf('n'));  // -1 (Java is case sensitive)

        String balloonColor = "Yellow";
        System.out.println(balloonColor.indexOf('l'));  // 2 (picks the first one)

        // Can get the index of a substring in the string
        String message = "This is my name";
        System.out.println(message.indexOf("is"));  // 2 (the index of the character in the substring)
    }

    public static void misc() {
        String name = "Nik";

        System.out.println(name.toLowerCase());     // nik
        System.out.println(name.toUpperCase());     // NIK

        // character at this index (opp of indexOf)
        System.out.println(name.charAt(0));     // 'N'
//        System.out.println(name.charAt(5)); // 5 is larger than length  // crashes

        // Does the string contain this substring (opp of substring)
        System.out.println(name.contains("ik"));    // true
        System.out.println(name.contains("nik"));    // f
        System.out.println(name.contains("N"));    // t
        System.out.println(name.contains(""));    // t (empty string is always a substring)
        System.out.println(name.contains("Nik"));    // t
        System.out.println(name.contains("Avi"));    // f
    }

    public static void equals() {
        int i = 1;
        int j = 2;
        int k = 1;
        boolean equal1 = i == j; // check equality is ==, single = only means assignment
        boolean equal2 = i == k; // check equality is ==, single = only means assignment

        System.out.println(equal1); // false
        System.out.println(equal2); // true

        // Same thing with String

        String name1 = "Nik";
        String name2 = "Nik";
        String name3 = "Avidant";
        boolean strEqual1 = name1 == name2; // DO NOT DO THIS
        boolean strEqual2 = name1 == name3; // DO NOT DO THIS

        System.out.println(strEqual2);  // false
        System.out.println(strEqual1);  // true(?)

        // primitives, objects
        // == -> they are the exact same value (you should never use this for objects)
        // equals() -> by default is ==, but you can have a "custom" equals method. Like toString
        boolean usingEquals1 = name1.equals(name2);
        boolean usingEquals2 = name1.equals(name3);

        System.out.println(usingEquals2);  // false
        System.out.println(usingEquals1);  // true

    }

    public static void compareTo() {
        // name of method is `compareTo`

        String name1 = "Nik";
        String name2 = "Avidant";
        int name1ComparedToName2 = name1.compareTo(name2);
        int name2ComparedToName1 = name2.compareTo(name1);
        int name1ComparedToName1 = name1.compareTo(name1);
        System.out.println(name1ComparedToName2);   // 13
        System.out.println(name2ComparedToName1);   // -13
        System.out.println(name1ComparedToName1);   // 0 -> both equal

        System.out.println("a".compareTo("b")); // 1
        System.out.println("a".compareTo("c")); // 2
        System.out.println("A".compareTo("N")); // -13

        // STRING1.compareTo(STRING2);
        //  |   if this string  |
        //  |    is "smaller"   |
        //  v                   v
        // negative          positive

        // STRING1.compareTo(STRING2) -> STRING1 - STRING2

        // compareTo, like equals and toString is something you'd write yourself (for a class you create)
        // but it doesn't have a default
    }

}
