package session3;

public class MyDogs {

    public static void main(String[] args) {

        Dog eclairs = new Dog("Eclairs", "Black");
        eclairs.setAgeInYears(0);       // eclairs.age
        eclairs.hasTail(true);
        eclairs.isHairy(false);
        eclairs.setWeight(11.5);

        System.out.println(eclairs);

        eclairs.getColor();
    }
}
