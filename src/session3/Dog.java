package session3;

public class Dog {
    private String name;
    private boolean hairy;
    private boolean tail;
    public String color;
    private double weight; // in lbs
    private int ageInMonths;

    public Dog(String name) {
        this.name = name; // setName(name);
    }

    public Dog(String name, String color) {
        this.name = name;
        this.color = color;
    }

    /*
    public OUTPUT-TYPE METHOD-NAME(INPUTS) {
        // DO THE JOB
        // IF NEEDED: RETURN THE VALUE
    }
     */

    public void setName(String name) {
        this.name = name;
    }

    public void setAgeInYears(int age) {
        ageInMonths = age * 12;
    }

    public void isHairy(boolean hairy) {
        this.hairy = hairy;
    }

    public void hasTail(boolean tail) {
        this.tail = tail;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public boolean getTail() {
        return tail;
    }

    public double getWeight() {
        return weight;
    }

    public double getWeightInOunces() {
        return weight * 16;
    }

    public boolean isHairy() {
        return hairy;
    }

    public boolean isTail() {
        return tail;
    }

    public int getAgeInYears() {
        return ageInMonths / 12;
    }

    public String getName() {
        return name;
    }

//    @Override
//    public String toString() {
//        return "Dog: weight is " + getWeight() + ", color is " + getColor() + ", age is " + getAgeInYears();
//    }

}
