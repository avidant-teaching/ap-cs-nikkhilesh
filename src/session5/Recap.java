package session5;

public class Recap {

    // String methods:
    // - length() -> returns the length of the string
    // - toLowerCase(), toUpperCase() -> return the string with the entire string in the upper/lower case
    // - indexOf() -> return the first index of the given input (char/string), returns -1. Index is 0 based
    // - charAt() -> return the character at the given index. Crashes if the index/position doesn't exist
    // - substring() -> returns the substring (part of the string) based on the input for start (and optional end).
    //                  start index is inclusive
    //                  end index is exclusive
    // - contains() -> returns whether or not the string contains the given substring
    //                  returns true iff the string contains the substring. returns false otherwise

    // Today: equals() and compareTo()
}
