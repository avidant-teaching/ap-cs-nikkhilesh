package session5;

public class MathClass {

    public static void main(String[] args) {
        // public -> everyone sees it and can use it
        // static -> it is the same for all instances of this class
        /*
            Integer i = 5;
            i.intValue();   // called the method on an instance
            Integer.MAX_VALUE;  //  not on a specific instance

            calling a static member -> ClassName.MEMBER-NAME (ClassName -> starts with a capital)
            calling a non-static member -> instanceName.MEMBER-NAME (instanceName -> starts with a small letter)
         */
        // void -> no output/nothing is returned
        // main -> name of the method, main method
        // String[] args -> user provided input. (Parameter)

        // everything(?) in the Math class is static
    }

    // MathClass.staticMethod();
    public static void staticMethod() {}

    // MathClass variableName = new MathClass();
    // variableName.nonStaticMethod();
    public void nonStaticMethod() {}

}
