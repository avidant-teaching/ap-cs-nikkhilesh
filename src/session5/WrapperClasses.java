package session5;

import java.util.Scanner;

public class WrapperClasses {

    public static void main(String[] args) {
        int maxValue = Integer.MAX_VALUE;
        System.out.println(maxValue);   // 2147483647
        int increasedMaxValue = maxValue + 1;
        System.out.println(increasedMaxValue);     // -2147483648 (same as Integer.MIN_VALUE) Called overflow

        int minValue = Integer.MIN_VALUE;
        System.out.println(minValue);   // -2147483648
        int decreasedMinValue = minValue - 1;
        System.out.println(decreasedMinValue);  // 2147483647 (same as Integer.MAX_VALUE) Called underflow
    }

    public static void parsingNumberFromString() {
        Scanner scan = new Scanner(System.in);
        System.out.println("What is your age in years?");
        String ageInYears = scan.nextLine();
        int ageInYearsAsInt = Integer.parseInt(ageInYears); // convert string to int
                                                            // double k = Double.parseDouble(string);
        int ageInFiveYears = ageInYearsAsInt + 5;
        System.out.println("Your age in 5 years will be: " + ageInFiveYears);
    }
    public static void learningWrappedClasses() {
        int i;
        double j;

        String str1 = "something";
        String str2 = new String("something");

        Integer int1 = new Integer(7);
        Integer int2 = 7;

        Double double1 = new Double(3.14);
        Double double2 = 3.14;
        double primitiveDouble  = 3.14;

        System.out.println(double2 == primitiveDouble);     // false (true?)
        System.out.println(double2.equals(primitiveDouble));    // true
//        System.out.println(primitiveDouble.equals(double2));    // doesn't run. You can only do .method on a object/non-primitive
        System.out.println(double2.compareTo(primitiveDouble)); // 0

        int someValue = int2.intValue();
        double someDoubleValue = double2.doubleValue();

    }
}
