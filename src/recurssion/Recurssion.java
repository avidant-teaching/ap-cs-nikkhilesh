package recurssion;

public class Recurssion {
	public static void main(String[] args) {
		int number = multipledByTwo(Integer.parseInt(args[0]));
		System.out.println(number);
		System.out.println(subStringFromNthCharacter("abcdefg", 4));
	}

	// factorial of 5 -> 5! -> 5 * 4 * 3 * 2 * 1
	// n! -> n * n-1 * n-2 .... * 1
	// n! -> n * (n-1)!
	// (n-1)! -> (n-1) * (n-2)!

	/*
		public static int factorial(int input) {
			int result = 1;
			for (int i = input; i > 0; i--) {
				result *= i;
			}
			return result;
		}
	*/

	public static int factorial(int input) {
		if (input == 0) {
			return 1;
		}
		return input * factorial(input - 1);
	}


/*
	public static int factorial(int input) {
		if (input == 0) {
			return 1;
		}

		return input * factorial(input - 1);
	}
*/

	// 1, 1, 2, 3, 5, 8, .....
	// fib(n) = fib(n - 1) + fib(n - 2)

	// fib(0) = 1
	// fib(1) = 1
	// fib(2) = 2
	// fib(3) = 3
	// fib(4) = 5
	// fib(5) = 8
	// fib(6) = 13

	public static int fib(int n) {
		if (n == 0 || n == 1) {
			return 1;
		}
		return fib(n - 1) + fib(n - 2);
	}


	// returns the sum of values from 0 to n
	// sum(0) -> 0
	// sum(1) -> 1
	// sum(2) -> 3
	// sum(3) -> 6
	public static int sum(int n) {
		if (n == 0) {
			return 0;
		}
		return n + sum(n - 1);
	}

	// return n * 2 (do this by adding 2 each time)
	// multipledByTwo(0) -> 0
	// multipledByTwo(1) -> 2
	// multipledByTwo(2) -> 4
	// multipledByTwo(n) -> 2 + multipledByTwo(n - 1)
 	public static int multipledByTwo(int n) {
		if (n == 0) {
			return 0;
		}
		return 2 + multipledByTwo(n - 1);
	}

	// subStringFromNthCharacter("abcdefg", 0) -> abcdefg
	// subStringFromNthCharacter("abcdefg", 4) -> efg

	// subStringFromNthCharacter("abcdefg", 4) -> efg
	// ...
	// end goal -> n = 0. When n == 0, input = result. result is efg. So input is efg. So progress will result in us calling
	// subStringFromNthCharacter("efg", 0)

	// difference between efg and abcdefg is that the first 4 letters are missing and the diff between 4 and 0 is 4
	// for us to print efg, we need to lose the first 4 letters

	// subStringFromNthCharacter("abcdefg", 4) -> efg
	// subStringFromNthCharacter("bcdefg", 3) -> efg
	// subStringFromNthCharacter("cdefg", 2) -> efg
	// subStringFromNthCharacter("defg", 1) -> efg
	// subStringFromNthCharacter("efg", 0) -> efg


	public static String subStringFromNthCharacter(String input, int n) {
		if (n == 0) {
			return input;
		}
		//                               removes the first character
		return subStringFromNthCharacter(input.substring(1), 

		//                              decrease n by 1
										n - 1);
	}

}
