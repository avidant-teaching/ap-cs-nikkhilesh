package recurssion;

import java.util.ArrayList;

public class BinarySearch {

    // returns true if the element is in arrayList
    public static boolean binarySearch(ArrayList<Integer> arrayList, int element) {
        // base case -> at what input will we do the smallest amount of work -> arrayList is empty
        if (arrayList.size() == 0) {
            return false;
        }
        return false;   // TODO

        /*

            - find the middle element (mid)
            - check if element is bigger, smaller or equal to mid
            - if  element > mid:
                - binarySearch on mid-endOfList
            - else if element < mid:
                - binarySearch on startOfList-mid
            - else (element == mid)
                - return true

         */
    }

}
