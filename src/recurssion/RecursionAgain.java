package recurssion;

public class RecursionAgain {

    // prints the string n times
    public static void printNTimesLoop(String string, int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(string);
        }
    }

    // prints the string n times
    // always prints something
    public static void printNTimesRecursion(String string, int n) {
        // a task is happening multiple times
        // what's the smallest task this method does -> printing "string" once
        // the smallest task is achieved at input -> n = 1
        // base case -> n = 1
        if (n == 1) {
            // the smallest task is done here
            System.out.println(string);
        } else {
            // what's the repetition -> printing
            // what's the difference between doing this task with input n vs n-1 -> printing one more/less time
            // recursion -> going step by step to the base case

            // all we have to do here, is the difference between task for n and task for n-1
            System.out.println(string);

            // now just tell java to do the rest of the work (for n-1)
            printNTimesRecursion(string, n - 1);
        }
    }

    // n is always non-negative (0 or positive)
    public static void printNTimesRecursionWithZero(String string, int n) {
        // smallest task this method can potentially do -> do nothing
        // at what input is the smallest task achieved -> n = 0
        if (n == 0) {
            // do nothing
        } else {
            System.out.println(string);
            printNTimesRecursionWithZero(string, n - 1);
        }
    }

    // return a + b. Use recursion
    // Assume a >= b >= 0
    public static int add1(int a, int b) {
        // smallest task -> return a
        // input for smallest task -> b = 0
        if (b == 0) {
            return a;
        }

        // adding 1 more to a and removing 1 from b
        return add1(a + 1, b - 1);
    }

    // return a + b. Use recursion
    // Assume a >= b >= 0
    public static int add2(int a, int b) {
        // smallest task -> return a
        // input for smallest task -> b = 0
        if (b == 0) {
            return a;
        }

        // adding 1 to the sum of a and b-1
        return 1 + add2(a, b - 1);
    }

    // factorial(n) = n * n-1 * n-2 * ... * 1
    // factorial(0) = 1
    // factorial(1) = 1
    // factorial(4) = 4 * 3 * 2 * 1 = 24
    // factorial(5) = 5 * 4 * 3 * 2 * 1 = 120
    public static int factorial(int n) {
        // smallest task -> returning 1 at n = 0
        if (n == 0) {
            return 1;
        }

        // difference between factorial(n) and factorial(n-1) -> multiplying by n
        // factorial(n) = n * factorial(n-1)
        // return -> n * factorial(n-1)
        return n * factorial(n - 1);
    }

    // palindrome -> reverse is the same as the original
    public static boolean isPalindrome(String string) {
        // base case -> 1 digit numbers -> return true
        // empty string is also a palindrome
        if (string.length() < 1) {
            return true;
        }

        // check the first and last element
        // if they are equal, run isPalindrome on number without the first and last element
        // if they are not equal, return false
        if (string.charAt(0) == string.charAt(string.length() - 1)) {
            return isPalindrome(string.substring(1, string.length() - 1));
        } else {
            return false;
        }
    }

    // HW:
    // Do both these methods using for loop and recursion

    // HW1
    // make a copy of str and return it
    public static String copyForLoop(String str) {
        return "";
    }

    // HW2
    // make a copy of str and return it
    public static String copyRecursion(String str) {
        return "";
    }

    // HW3
    // return the first upper case character
    // if no upper case character exists, return '0'
    public static char firstUpperCaseCharacterForLoop(String str) {
        char noUpperCaseExists = '0';
        // use str.charAt(index) to get the character at a specific index
        // use Character.isUpperCase(character) (returns boolean) to find out if a character is uppercase

        return noUpperCaseExists;
    }

    // HW4
    // return the first upper case character
    // if no upper case character exists, return '0'
    public static char firstUpperCaseCharacterRecursion(String str) {
        char noUpperCaseExists = '0';
        // use str.charAt(index) to get the character at a specific index
        // use Character.isUpperCase(character) (returns boolean) to find out if a character is uppercase

        return noUpperCaseExists;
    }

}
