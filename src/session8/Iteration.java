package session8;

public class Iteration {

    public static void main(String[] args) {
        printAllPositiveMultiplesOfSevenLessThanHundred();
    }

    public static void printNumbersOneToTenOld() {
        /*
            OUTPUT:
            1
            2
            3
            4
            5
            6
            7
            8
            9
            10
         */

        System.out.println(1);
        System.out.println(2);
        System.out.println(3);
        System.out.println(4);
        System.out.println(5);
        System.out.println(6);
        System.out.println(7);
        System.out.println(8);
        System.out.println(9);
        System.out.println(10);
    }

    public static void learningLoops() {
        // We need to print 1 to 10

//        for (int i = 1; i <= 10; i += 2) {
//            System.out.println(i + ": Nik");
//        }
//
//        /*
//            i = 1, true, "1 Nik", i = 3
//            i = 3, true, "3 Nik", i = 5
//            i = 5, true, "5 Nik", 7
//            i = 7       "7 Nik"
//            i = 9, true, "9 Nik", i = 11
//            i = 11, false
//         */

//        for (int i = 1; i <= 10; i += 10) {
//            System.out.println(i + ": Nik");
//        }
//
//        /*
//            i = 1, true, "1 Nik", i = 11
//            i = 11, false
//         */

        for (int i = 10; i > 0; i--) {
            System.out.println(i + ": Nik");
        }
        /*
            10, true, 10 Nik, 9
            9 true, 9 Nik, 8
            8 true, 8Nik, 7
            ...
            2, true, 2 Nik, 1
            1, true, 1 Nik, 0
            0, false
         */

        System.out.println("I am done");
    }

    public static void loopOverString() {

        String name = "Nik";

        // .charAt
        // .length
        // 0 -> 2. length, 3

        // for (initialize ; check state ; update) {}

        // loop over the letters of the name and print each (on new lines)
        for (int i = 0; i < name.length(); i++) {
            System.out.println(name.charAt(i));
        }

        // print the letters of variable name in reserve order

        // k -> 2
        // ...
        // N -> 0

        // startAt -> last index (last index is always 1 less than the length), endAt -> 0
        // update -> i--

        for (int i = name.length() - 1; i >= 0; i--) {
            System.out.println(name.charAt(i));
        }


    }

    public static void printAllPositiveMultiplesOfSevenLessThanHundred() {
//        for (int i = 100; i >= 0; i -= 7) { // A smart start, but 100 is not a multiple of 7. Let's try this from 1 to 100

        // 0, 7, 14, 21, ...., 98
        // 7, 14, 21, 28....., 98

        // 100% correct. Best way to do it
        for (int i = 7; i < 100; i += 7) {
            System.out.println(i);
        }

//        Given this format, fill in the blanks to make the loop work the same way as above
//        for (int i = 1; ___; i++) {
//            System.out.println(___);
//        }

        // avoid using 98 and other constants so that it is easy to reuse the code

        // It's not the right way to do it. Just for academic purposes
        for (int i = 1; i * 7 < 100; i++) {
            System.out.println(i * 7);
        }

        // It's not the right way to do it. Just for academic purposes
        for (int i = 1; i < 100; i++) {
            if (i % 7 == 0) {
                System.out.println(i);
            }
        }
    }

}
