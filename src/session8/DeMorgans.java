package session8;

public class DeMorgans {

    public static void main(String[] args) {

        int number1 = 7;
        int number2 = 9;


        // Truth Tables
        /*
            A && B  vs A & B

            A & B -> not smart
            A && B -> smart/lazy it'll stop evaluating if the first value is false -> false

              A  |  B  | A && B
            -----|-----|-------
            true |true | true
            false|false| false
            true |false| true
            false|true | false



            A || B  vs A | B

            A | B -> not smart
            A || B -> smart/lazy it'll stop evaluating if the first value is true -> return true

              A  |  B  | A || B
            -----|-----|-------
            true |true | true
            false|false| false
            true |false| true
            false|true | true
         */


        /*
            DeMorgan's Law ->

            (!A && !B) is the same as !(A || B)
            (!A || !B) is the same as !(A && B)

              A  |  B  | A || B | !(A || B) | !A   | !B    | (!A && !B)
            -----|-----|------- |-----------|------|-------|------------
            true |true | true   | false     | false| false | false
            false|false| false  | true      | true | true  | true
            true |false| true   | false     | false| true  | false
            false|true | true   | false     | true |false  | false

         */

        boolean bothAreEven1 = number1 % 2 != 1 && number2 % 2 != 1;  // false

        // (!A && !B) is the same as !(A || B)

        /*
            !A && !B -> number1 % 2 != 1 && number2 % 2 != 1
            !A -> number1 % 2 != 1
            !B -> number2 % 2 != 1
            A -> number1 % 2 == 1
            B -> number2 % 2 == 1
            !(A || B) -> !(number1 % 2 == 1 || number2 % 2 == 1)
         */


        boolean bothAreEven2 = !(number1 % 2 == 1 || number2 % 2 == 1);


        System.out.println(bothAreEven1);
        System.out.println(bothAreEven2);

    }

    public static void recap() {
        int number1 = 7;
        int number2 = 9;

        if (/*booleanExpression*/ number1 % 2 == 0 && number2 % 2 == 0) {
            System.out.println("Even");
        } else {
            System.out.println("Both are not even");
        }

    }

    public static void misc(int number1, int number2) {

        if (!(number1 % 2 == 1 || number2 % 2 == 1)) {
            // set of numbers A
        } else {
            // set of numbers B
        }

        if (number1 % 2 == 0 && number2 % 2 == 0) {
            // set of numbers A
        } else {
            // set of numbers B
        }

    }
}
