package twodarrays;

import java.util.Arrays;

public class Loops {

    public static void main(String[] args) {
        int[][] data = {{5, 4, 67}, {4, 7, 43}, {54, 32, 5}};
        sumOfAllElements(data);
    }

    public static void basicForLoop() {
        int[][] dates = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {0, 1, 2}};

        // HW
        // write a for loop that prints all the elements in the format:
        // [0][0] -> dates[0][0]
        // [0][1] -> dates[0][1]
        // [0][2] -> ?
        // ...
        // [4][4] -> ?

//        for (int rowNumber = 0; rowNumber < dates.length; rowNumber++) {
//            int[] row = dates[rowNumber];
//            for (int colNumber = 0; colNumber < row.length; colNumber++) {
//                System.out.println(row[colNumber]);
//            }
//        }

        // replace row with dates[rowNumber]
        for (int rowNumber = 0; rowNumber < dates.length; rowNumber++) {
            for (int colNumber = 0; colNumber < dates[rowNumber].length; colNumber++) {
                System.out.println("[" + rowNumber + "][" + colNumber + "] -> " + dates[rowNumber][colNumber]);
            }
        }

    }

    public static void mixingWithReverseForLoop() {
        int[][] dates = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {0, 1, 2}};

        // write a for loop that prints all the elements in the format:
        // [0][2] -> dates[0][2]
        // [0][1] -> dates[0][1]
        // [0][0] -> dates[0][0]
        // [1][2] ->
        // [1][1] ->
        // [1][0] ->
        // ...
        // [3][0] -> ?

        for(int rowNumber = 0; rowNumber < dates.length; rowNumber++) {
            int[] row = dates[rowNumber];
            // print row in reverse
            for(int colNumber = row.length - 1; colNumber >= 0; colNumber--) {
                System.out.println("[" + rowNumber + "][" + colNumber + "] -> " + row[colNumber]);
            }
        }
    }

    public static void enhancedForLoop() {
        int[][] dates = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {0, 1, 2}};

        // write a for loop that prints all the elements in the format:
        // dates[0][0]
        // dates[0][1]
        // dates[0][2]
        // ...

        // Syntax for enhanced for loop:
        // String[] names = ...;
        // for (String s : names) {
        //   System.out.println(s);
        // }
        // names -> array of type String

        for (int[] row : dates) {
            // int[] -> array of type int
            // int[][] -> array of type int[]
            // dates -> array of type int[]

            // now it's just a simple problem of printing every element inside row
            for (int column : row) {
                System.out.println(column);
            }
        }

    }

    public static void sumOfAllElements(int[][] data) {
        // calculate the sum of all elements in data using for loops and print it
        // then repeat using enhanced for loop and print the sum

        int sum1 = 0;
        int sum2 = 0;

        for (int rowNum = 0; rowNum < data.length; rowNum++) {
            int[] row = data[rowNum];
            for (int colNum = 0; colNum < row.length; colNum++) {
                sum1 += row[colNum];
            }
        }

        System.out.println(sum1);

        for (int[] row : data) {
            for (int col : row) {
                sum2 += col;
            }
        }

        System.out.println(sum2);
    }

    public static void rowTotals(int[][] data) {
    
        // {{5, 4, 67}, {4, 7, 43}, {54, 32, 5}}
        // Step 1: Look at row 0 {5, 4, 67}
            // sum = 0
            // Look at each element of the row and add their value to sum
            // sum = 76
            // print ("row 0 -> 76")
        // Step 2: Look at row 1 {4, 7, 43}
            // sum = 0
            // Look at each element of the row and add their value to sum
            // sum = 54
            // ...


        // FOR LOOPS

        for (int i = 0; i < data.length; i++) {
            int[] row = data[i];
            // question -> print the sum of elements in row
            int sum = 0;
            for (int j = 0; j < row.length; j++) {
                sum = sum + row[j];
            }
            System.out.println("row " + i + " -> " + sum);
        }


        // FOR-EACH/ENHANCED FOR LOOPS

        /*
            for (type_of_array variable_name_representing_element : array_name) {

            }

            // eg with String[] names
            for (String name : names) {

            }

        Step 1:
            for (??? ??? : data) {

            }

        data -> int[][]
        each element in data is of what type? -> int[]

        Step 2:
            for (int[] ??? : data) {

            }

        What does each int[] represent -> row

        Step 3:
            for (int[] row : data) {
                ...
            }

        */

        for (int[] row : data) {
            int sum = 0;
            for (int number : row) {
                sum += number;
            }
            System.out.println("total -> " + sum);
        }

    }

}
