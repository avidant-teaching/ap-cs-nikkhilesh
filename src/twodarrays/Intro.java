package twodarrays;

public class Intro {

    public static void main(String[] args) {
        settingAndGettingValues();
    }


    public static void storage() {
        // Array -> a space in memory to store a specific number of elements of a specific type

        // what if I want the type to be an array -> I want an array of array

        /*
        [ . . . . . . . . . . . (array starts here).....(array ends here) . . . . . . ]

        Memory we have
        [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]

        a boolean array of length 5
        [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
         0   1   2   3   4  [no longer part of the array]


        memory starts here
        // a 2d array of boolean with length 5
        [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
         0   1   2   3   4
         |   |   |   |   |
         v   v   v   v   v
        [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
        [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
        [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
        memory ends here

          0    1   2   3   4   5   6
A
B
C        [P0] [P1] [P2] [P3] [P4] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
...
P        [true] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
Q        [false] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
R        [true] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]

        array1 -> [P0] [Q0] [R0]
         */
    }

    public static void declaring() {
        // declare an int with variable name age and set it to value 7
        int age = 7;

        // declare a String with variable name city and set it to Seattle
        String city = new String("Seattle");


        // array of type int with variable name dates with length 5
        int[] dates = new int[5];

        // declaration -> int[] dates (type variablename)
        // initialization -> new int[5]

        // declare a 2d array of int with variablename weather
        int[][] weather;

        // initialize weather to have 2 rows and 3 columns
        weather = new int[2][3];
    }

    public static void settingAndGettingValues() {
        int[][] weather = new int[12][9];

        // set the value in the 1st row and 1st column of weather to 7
        weather[0][0] = 7;

        // set the value in the 1st row and 3rd column of weather to 17
        weather[0][2] = 17;

        // set the value in the 2nd row and 1st column of weather to 27
        weather[1][0] = 27;

        // print the value at row 3, column 5
        System.out.println(weather[2][4]);      // 0 -> when you create an array, it sets the values to the default

        // print the value at row 2, column 1
        System.out.println(weather[1][0]);      // 27
    }

    public static void initializerLists() {
        int[] dates = {1, 3, 5, 4, 6, 5, 5, 4, 5};
        String[] names = {"", "", "", ""};

        String[][] seatingChart = {{"a", "b"}, {"c", "d"}, {"e", "f"}};

        System.out.println(seatingChart[1][1]);     // d
        System.out.println(seatingChart[0][1]);     // b
        System.out.println(seatingChart[1][0]);     // c
    }


}
