package session6;

public class MathClass {

    // for all methods, classname: Math
    // all methods are static in the Math class

    // abs -> return the absolute value
    // random -> returns a random double from 0.0 to 1.0 (1.0 is excluded)
    // sqrt -> returns the positive square root of the given input
    // pow(x, y) -> returns the value of x to the power y

    public static void main(String[] args) {
        power();

    }

    public static void power() {
//        method name: pow
        // parameters: double, double
        // return: double

        // call pow with parameters 7, x. Store it in variable z
        double x = 3;

        double z = Math.pow(7, x);

        System.out.println(z);
    }

    public static void squareroot() {
        // method name: sqrt
        // parameter: double
        // return: double

//        get the result of sqrt on variable x and store it in variable z
        double x = 2.;

        double z = Math.sqrt(x);

        System.out.println(z);

    }

    public static void learningRandom() {
        // method name: random
        // parameter: none
        // return: double

        // get the result of random and store it in a variable k
        // print the result of k * 5

        double k = Math.random();
//        System.out.println(k * 5);

        // random double between 0 and 10 (10 excluded)
        System.out.println(Math.random() * 10);

        // random int between 0 and 10 (10 excluded)
//        System.out.println((int) Math.random() * 10);   // will not work
        System.out.println((int) (Math.random() * 10));

        // roll a dice (1 to 6)
//        System.out.println((int) (Math.random() * 6)); WILL NOT WORK
//        System.out.println((int) (Math.random() * 6)); 0, 1, 2, 3, 4, 5 vs 1, 2, 3, 4, 5, 6
//        0 1 2 3 4 5
//        1 2 3 4 5 6
        System.out.println("Roll a dice");
        System.out.println((int) (Math.random() * 6) + 1);

        // Numbers between a and b (b exclusive) // a = 1, b = 7
        // number of possibilities -> ( b - a)    6
        // smallest number you can accept -> a    1

//        ((int) Math.random() * number_of_possibilities) + smallest_number
//        ((int) Math.round() * (b - a)) + a

//        random number between 7 and 10
//        Math.random()     // random number from 0 to 1.0 (exclusive)
//        (int) (Math.random() * 3)    // random number from 0 to 3.0 (exclusive) -> multiplying extends the range
        int z = (int) (Math.random() * 3) + 7;  // random number from 7 to 10.0 (exclusive) -> adding shifts the range
    }

    public static void absDouble() {
        // method name: abs
        // parameter: double
        // return: double

        // get the value of calling abs on -3.14, store the result in variable k

        double k = Math.abs(-3.14);

        // double k = -3.14
        // Math.abs(-3.14);
        // int k = (int) Math.abs(-3.14);

    }

    public static void absInt() {
        // method name: abs
        // parameter: int
        // return: int

        // print the value of calling abs with input -3

        System.out.println(Math.abs(-3));
        System.out.println(Math.abs(3));

        // get the result of calling abs on -3, store the result in variable z
        // get the result of calling abs on -3 : Math.abs(-3)
        // int z = result of calling abs on -3
        // int z = Math.abs(-3);

        int z = Math.abs(-3);

    }


}
