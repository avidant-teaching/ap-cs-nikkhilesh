package session6;

public class Recap {

    public static void main(String[] args) {
        // public: everyone can see and use it
        // static: independent of any instance of this class
        // void: returns nothing (no output)
        // main: name of the method, it's the method that runs when you run this class
        // String[] args: Parameter (input)
    }

    public static void staticVsNonStatic() {

        // classname: SomeClass
        // static method: staticMethod()
        // non-static method: nonStaticMethod()

        // static -> independent of instances
        SomeClass.staticMethod();

        // non-static -> dependent on a specific instance
        /*
            methodName -> length()
            method belongs to class -> String
            method is called on instance with type -> String
         */

        String name = new String("Nik");
        name.length();

        /*
            methodName -> nonStaticMethod()
            method belongs to class -> SomeClass
            method is called on instance with type -> SomeClass
         */

        // we need an instance of SomeClass type
        SomeClass someClass = new SomeClass();
        someClass.nonStaticMethod();

    }

    private static class SomeClass {
        private static void staticMethod() {
            System.out.println("This is a static method");
        }

        private void nonStaticMethod() {
            System.out.println("This is a non-static method");
        }
    }

}
