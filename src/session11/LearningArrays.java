package session11;

import java.util.Arrays;

public class LearningArrays {

    public static void main(String[] args) {
        partialForLoop();
    }

    public static void partialForLoop() {
        String[] countries = {"USA", "Canada", "UK", "Germany", "Russia", "India", "Italy", "Brazil"};

        // Print the first 5 countries. You can assume there are always atleast 5 countries
        for (int i = 0; i < 5; i++) {
            System.out.println(countries[i]);
        }

        // Change the first 5 countries and set them all to "Brazil"
        for (int i = 0; i < 5; i++) {
            countries[i] = "Brazil";
        }

        for (int i = 0; i < 5; i++) {
            System.out.println(countries[i]);
        }
    }

    public static void reverseForLoopOverAnArray() {
        String[] countries = {"USA", "Canada", "UK", "Germany"};

        // Print every value in the countries array in reverse order. Each value should be in a newline
        for (int i = countries.length - 1; i >= 0; i--) {
            System.out.println(countries[i]);
        }
    }

    public static void forLoopOverAnArray() {
        String[] countries = {"USA", "Canada", "UK", "Germany"};

        // Print every value in the countries array. Each value should be in a newline
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);
        }

        String country = "USA";
        for (int i = 0; i < country.length(); i++) {
            System.out.println(country.charAt(i));
        }
        // A string is a sequence of characters. It practically is an array of characters
    }

    public static void getARandomCountry() {
        String[] countries = {"USA", "Canada", "UK", "Germany"};

        // Write the code to randomly print one country

        /*
            Step 1: Math.random() returns values from 0 (inclusive) to 1 (exclusive)
            Step 2: Values we want are 0 to 3
            Step 3: multiple result of Math.random() with the length of the array. This will give us values from
                    0 (inclusive) to 4 (exclusive)
            Step 4: We convert the double to int by casting
         */

        // 0.37873648 -> 0
        // 1.2342897 -> 1
        // 2.89798 -> 2
        // 3.98769879 -> 3
        // 3.99999999999999 -> 3

        int randomIndex = (int) (Math.random() * countries.length);     // The parentheses are required
//        int randomIndex = (int) Math.random() * countries.length;  // (int) Math.random is always 0
        System.out.println(countries[randomIndex]);
    }

    public static void editingAnArray() {
        String[] namesOfStudents = {"Nik", "Avidant", "Emily"};

        // change the name in the second position to "Bob"
        namesOfStudents[1] = "Bob";

        // change the name in the last position to "John"
        editAnArray2(namesOfStudents);
    }

    public static void editAnArray2(String[] namesOfStudents) {
        // change the name in the last position to "John"
        namesOfStudents[namesOfStudents.length - 1] = "John";
    }

    public static void settingValuesInTheArray() {
        String[] namesOfStudents = new String[5];

        // Set the values of namesOfStudents to be {"Nik", "Avidant", "Emily", "John", "Bob"}
        namesOfStudents[0] = "Nik";
        namesOfStudents[1] = "Avidant";
        namesOfStudents[2] = "Emily";
        namesOfStudents[3] = "John";
        namesOfStudents[4] = "Bob";

        System.out.println(Arrays.toString(namesOfStudents));
    }

    public static void gettingValuesFromArrays() {
        String[] namesOfStudents = {"Nik", "Avidant", "Emily"};
        String firstName = namesOfStudents[0];      // Nik
        String secondName = namesOfStudents[1];     // Avidant
        String thirdName = namesOfStudents[2];      // Emily

//        String thisIndexDoesntExist = namesOfStudents[3];   // throws an ArrayIndexOutOfBounds

        int[] dates = {23, 3, 27, 12, 18, 7};

        // Print the third date in the array dates
        System.out.println(dates[2]);

    }

    public static void arrayLength() {
        // Length:
        // 1. To know how many values you can access
        // 2. For a for loop, to go value by value

        String str = "Nik";
        int lengthOfString = str.length();

        String[] namesOfStudents1 = new String[5];
        int lengthOfArray = namesOfStudents1.length;

        System.out.println(lengthOfArray);      // 5 even though we don't explicitly set any of the values

        String[] namesOfStudents2 = {"Nik", "Avidant", "Emily"};
        System.out.println(namesOfStudents2.length);    // even though we didn't explicitly set it, it's 3
    }

    public static void creatingArraysWithValues() {
        String[] namesOfStudents1 = new String[5];
        // the values inside the array are all empty/not set

        String[] namesOfStudents2 = {"Nik", "Avidant", "Emily"};    // no need to explicitly set the size
        // The values are Nik, Avidant and Emily

        int[] attendance = {2,4,2,5,7,5,7,9,8};
    }

    public static void intArray() {
        // int array
        // size 12
        // represents attendance by month
        int[] attendance = new int[15];
    }

    public static void stringArray() {
        // This declares an array of Strings
        String[] names;     // This represents a group/list/array of String

        // To initialize an array:
        // 1. You need the type of the Array (In this case String)
        // 2. You need the size of the Array (Since we're giving it space in memory we need to know how much)
        names = new String[5];      // 5 is the size of the Array

        // Declare and initialize together
        String[] namesOfStudents = new String[5];

    }
}
