package session10;

public class UsingAirplane {

    // What does an Airplane class represent? an airplane

    public static void main(String[] args) {
        usingClasses();
    }

    public static void learningUsingClasses() {
        String manufacturer = "Airbus";
        int year = 2015;
        double weight = 3.413;


        // How do we create a new Airplane1
        Airplane1 airplane1 = new Airplane1();

        // How do we create a new Airplane2 (which requires 2 inputs: manufacturer and year)
        Airplane2 airplane2 = new Airplane2(manufacturer, year);
        // Cannot switch the order
        // Airplane2 airplane2 = new Airplane2(year, manufacturer);
    }

    public static void usingClasses() {
        String manufacturer = "Airbus";
        int year = 2015;
        double weight = 3.413;

        // Create an Airplane1 object with the manufacturer, year and weight set as above
        Airplane1 airplane1 = new Airplane1();
        airplane1.setManufacturer(manufacturer);
        airplane1.setYear(year);
        airplane1.setWeight(weight);

        // Create an Airplane2 object with the manufacturer, year and weight set as above
        Airplane2 airplane2 = new Airplane2(manufacturer, year);
        airplane2.setWeight(weight);

        // Print the weight and year of both airplanes
        System.out.println(airplane1.getWeight());
        System.out.println(airplane1.getYear());

        System.out.println(airplane2.getWeight());
        System.out.println(airplane2.getYear());
    }
}
