package session10;

// Just a simple airplane class that takes no input when being created
public class Airplane1 {
    private String manufacturer;
    private double weight;
    private int passengers;
    private boolean grounded;
    private int year;

    public void setManufacturer(String m) {
        manufacturer = m;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setWeight(double w) {
        weight = w;
    }

    public double getWeight() {
        return weight;
    }

    public void setPassengers(int p) {
        passengers = p;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setGrounded(boolean g) {
        grounded = g;
    }

    public boolean getGrounded() {
        return grounded;
    }

    public void setYear(int y) {
        year = y;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        String ground = "";
        if(grounded) {
            ground = "The plane is grounded ";
        }
        else {
            ground = "The plane is not grounded ";
        }
        return "Airplane was manufacturered by " + manufacturer + " in " + year + " and weight " + weight + "." + ground + "and can hold " + passengers + " passengers.";
    }
}