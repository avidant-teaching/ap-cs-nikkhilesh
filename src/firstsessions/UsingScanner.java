package firstsessions;

import java.util.Scanner;

public class UsingScanner {

    public static void main(String[] args) {
        System.out.println("Hey! What's your name?");
        Scanner lineScanner = new Scanner(System.in);
        String name = lineScanner.nextLine();
        System.out.println("Welcome " + name + "!");
    }

}
