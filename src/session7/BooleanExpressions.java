package session7;

import session3.Dog;

import java.util.Scanner;

public class BooleanExpressions {

    public static void main(String[] args) {
        exitingTheIf();
    }

    // ask the user for an integer as input, check if that input is one and if it is, say welcome,
    // if it's not say goodbye
    public static void isTheInputOne() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter an integer");
        int input = scan.nextInt();

        if (input == 1) {
            // do what we want to do if (input == 1) is true
            System.out.println("Welcome");
        } else {
            // do what we want to do if (input == 1) is false
            System.out.println("Goodbye");
        }
    }

    // ask the user for an integer as input, check if that input is odd and if it is, say welcome,
    // if it's not say goodbye
    public static void isTheInputOdd() {
        /*
            +, -, *, /, %
            % -> remainder
            Checking if a number is even: number % 2 == 0
            Checking if a number is odd: number % 2 != 0 OR number % 2 == 1
         */

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter an integer");
        int input = scanner.nextInt();

        if (input % 2 != 0) {
            System.out.println("Welcome");
        } else {
            System.out.println("Goodbye");
        }
    }

    // ask the user for a password, check if password input is "hello world" and if it is, say welcome,
    // if it's not say goodbye
    public static void isThePasswordCorrect() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the password");
        String input = scan.nextLine();

        // == *does not* work with non-primitive data types
        if (input.equals("hello world")) {
            System.out.println("Welcome");
        } else {
            System.out.println("Goodbye");
        }
    }

    // ask the user for a password, check if password input is "hello world" and if it is not suggest a password reset
    public static void suggestPasswordReset() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the password");
        String input = scan.nextLine();

        // == *does not* work with non-primitive data types
        if (input.equals("hello world")) {
            // do nothing
        } else {
            System.out.println("We suggest a password reset");
        }
    }

    public static void learningBangOperator() {

        boolean result = 1 == 2;    // result = false
        boolean bangOperator = !result;     // bangOperator = true

        System.out.println(!true);          // false
        System.out.println(!false);         // true
        System.out.println(!(1 == 2));      // true
        System.out.println(!(1 == 1));      // false
    }

    // ask the user for a password, check if password input is "hello world" and if it is not suggest a password reset
    public static void betterSuggestPasswordReset() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the password");
        String input = scan.nextLine();

        if (!input.equals("hello world")) {
            System.out.println("We suggest a password reset.");
        }
    }

    // print "first number is greater" if the first is greater, "sec...." if the second is greater, "both are equal" if
    // both are equal
    public static void greaterThan() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 2 numbers");
        int firstNumber = scan.nextInt();
        int secondNumber = scan.nextInt();

        if (firstNumber > secondNumber) {           // first condition
            System.out.println("First number is greater");
        } else if (secondNumber > firstNumber) {            // second condition
            System.out.println("Second is greater");
        } else {                        // You can also use an else if here but it's not needed so you shouldn't use it
            System.out.println("Both are equal");
        }

        // else if -> the first and second conditions are not mutually exclusive

        // Another way to do it (not generally recommended)
//        if (firstNumber > secondNumber) {
////            ...
//        }
//        if (secondNumber > firstNumber) {
////            ...
//        }
//        if (firstNumber == secondNumber) {
////            ...
//        }


        // Not recommended in this case
//        if (firstNumber > secondNumber) {
//            System.out.println("First number is greater");
//        } else {
//            if (secondNumber > firstNumber) {
//                System.out.println("Second is greater");
//            } else {
//                System.out.println("Both are equal");
//            }
//        }

    }

    public static void aliasing() {
        Dog dog1 = new Dog("dog");
        Dog dog2 = new Dog("dog");
        Dog dog3 = new Dog("dog");

        /*
            dog1 -> 0x1?????? (some location in memory)
            dog2 -> 0x2?????? (some location in memory)
            dog3 -> 0x3?????? (some location in memory)
         */

        System.out.println(dog1 == dog2);   // false
        System.out.println(dog3 == dog2);   // false
        System.out.println(dog1 == dog3);   // false

        Dog dog4 = dog1;
        /*
            dog4 -> 0x1?????? (same location as dog1)
         */
        Dog dog5 = dog4;
        /*
            dog5 -> 0x1?????? (same location as dog4) (by extension, same location as dog1)
         */
        // dog1, dog4, dog5

        System.out.println(dog1 == dog4);   // true
        System.out.println(dog1 == dog5);   // true
        System.out.println(dog4 == dog5);   // true

        // A == B is always the same as B == A

        System.out.println(dog2 == dog4);   // false
    }

    public static void complexAliasing() {
        Dog dog1 = new Dog("D1");
        Dog dog2 = new Dog("D2");

        /*
            dog1 -> 0x1?????? (some location in memory)
            dog2 -> 0x2?????? (some location in memory)
         */

        Dog dog3 = dog1;
        /*
            dog1, dog3 -> 0x1?????? (some location in memory)
            dog2 -> 0x2?????? (some location in memory)
         */

        dog1 = dog2;
        /*
            dog3 -> 0x1?????? (some location in memory)
            dog1, dog2 -> 0x2?????? (some location in memory)
         */

        /*
            Which variables represent the same value:
            - dog3          -> D1
            - dog1, dog2    -> D2
         */

        System.out.println(dog1);
        System.out.println(dog2);
        System.out.println(dog3);

    }

    public static void exitingTheIf() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter a number");
        int input = scanner.nextInt();
        if (input % 2 == 0) {   // if the input is even
            System.out.println("Even");
        } else {
            System.out.println("Odd");
        }
        System.out.println("Thanks for using my program");
    }




}
