package session7;

public class CombiningBooleanExpressions {

    public static void main(String[] args) {
        ifTwoConditionsAreTrue();
    }

    public static void ifTwoConditionsAreTrue() {
        int number1 = 4;
        int number2 = 5;

        // print "hello world" if number1 AND number2 are both even
        if (number1 % 2 == 0 && number2 % 2 == 0) {
            System.out.println("Hello World");
        }

        // print "hello world again" if number1 OR number2 are even (either are even)
        if (number1 % 2 == 0 || number2 % 2 == 0) {
            System.out.println("Hello World again");
        }

    }
}
