package quizes;

import java.util.Scanner;

public class Quiz1 {

    public static void main(String[] args) {
        q10();
    }

    // For each of the following, describe the type, list the possible values it can hold, and state whether
    // it is a primitive type or not
    public static void q1() {
        /*
            - int: primitive variable, represents an integer, can hold up to 32 bits,
                    Integer.MIN_VALUE to Integer.MAX_VALUE
            - double: primitive variable, represents an integer with a decimal point, up to 64 bits
                    Instead say real number, floating number, any number with a decimal point
            - char: primitive variable, holds a single character
            - Double: same as double but its a class
                    Wrapper around double to provide class/data type functionality
            - String: Non-primitive data type, holds a string or words
                    sequence of characters, zero or more characters
            - boolean: primitive, holds true or false
            - Boolean: wrapper class for boolean
            - long:
                primitive, holds integers with a larger range than int
            - Scanner: Class that scans user input
        */
    }

    // What is the difference between == and .equals()
    public static void q2() {
        /*
            .equals is for non-primitive, == is for primitive

            String a = "?";
            String b = "?";
            boolean equal = a == b;

            == checks if the reference is the same while .equals can be used on classes/objects to check if the
            value is the same
         */
    }

    /*
    Write a class that represents an airplane. Provide methods for a user to be able to get and set the following
    details:
    - manufacturer
    - weight
    - number of passengers it can hold
    - if the plane is currently grounded
    - Year it was manufactured
     */
    public static void q3() {
        // created class airplane
    }

    public static class Airplane {
        private String manufacturer;
        private double weight;
        private int passengers;
        private boolean grounded;
        private int year;

//        public String setManufacturer(String m) {
        public void setManufacturer(String m) {
            manufacturer = m;
        }

        public String getManufacturer() {
            return manufacturer;
        }

//        public double setWeight(double w) {
        public void setWeight(double w) {
            weight = w;
        }

        public double getWeight() {
            return weight;
        }

//        public int setPassengers(int p) {
        public void setPassengers(int p) {
            passengers = p;
        }

        public int getPassengers() {
            return passengers;
        }

//        public boolean setGrounded(boolean g);
        public void setGrounded(boolean g) {
            grounded = g;
        }

        public boolean getGrounded() {
            return grounded;
        }

        public void setYear(int y) {
            year = y;
        }

        public int getYear() {
            return year;
        }

        @Override
        public String toString() {
            String ground = "";
//            if(grounded == true) {
            if(grounded) {
                ground = "The plane is grounded ";
            }
            else {
                ground = "The plane is not grounded ";
            }
            return "Airplane was manufacturered by " + manufacturer + " in " + year + " and weight " + weight + "." + ground + "and can hold " + passengers + " passengers.";
        }

        // Another way to do toString
        public String toString2() {
            String ground = "The plane is not grounded ";
            if (grounded) {
                ground = "The plane is grounded ";
            }
            return "Airplane was manufacturered by " + manufacturer + " in " + year + " and weight " + weight + "." + ground + "and can hold " + passengers + " passengers.";
        }
    }


    // Update the airplane class created in Q6 such that if an instance of the class is printed, it prints as follows:
    // "Airplane was manufacturered by ___ in ___ and weight ___. The plane is [not] grounded and can hold ___ passengers."
    public static void q4() {
        // Added to Airplane class above
    }

    // Create a program that creates an airplane based on input provided by the user, and then prints the created
    // airplane
    public static void q5() {
        // This would be in public static void main
        Scanner scan = new Scanner(System.in);
        System.out.println("Manufacturer: ");
        String m = scan.nextLine();         // avoid nextLine in combination with next/nextInt/next???
        System.out.println("Weight: ");
        double w = scan.nextDouble();
        System.out.println("Number of Passengers: ");
        int p = scan.nextInt();
        System.out.println("Was the plane grounded(true or false): ");
        boolean g = scan.nextBoolean();
        System.out.println("Year: ");
        int y = scan.nextInt();
//        Airplane a1 = new Airplane(m,w,p,g,y);
        Airplane a1 = new Airplane();
        a1.setGrounded(g);
        a1.setManufacturer(m);
        a1.setPassengers(p);
        a1.setWeight(w);
        a1.setYear(y);
        System.out.println(a1);

        // better variable names -> name, weight, passengerCount, grounded, year, airplane
    }

    // What is a wrapper class? Give 2 examples
    public static void q6() {
        // a Wrapper classes used primitive data types as objects, Boolean, Double

        // a wrapper class is used to provide object functionality to primitives
    }

    /*
    Write a method that takes a double and an int as input and does the following:
    - prints the square root of the int
    - prints the value of the double raised to the int
    - Generates a random number between 19 and 25 (exclusive)
    - prints the absolute value of the double
     */
    public static void q7(double d, int i) {
        System.out.println(Math.sqrt(i));
        System.out.println(Math.pow(d, i));
        int r1 = (int)((Math.random()*6) + 19);
        int r2 = (int) (Math.random() * 6) + 19;
        System.out.println(Math.abs(d));
    }

    /*
    Write a program that takes as input from the user their name, and does the following:
    - Prints the first character of the name
    - Prints the user's name in all capitalized letters
    - Prints the length of the name
    - Checks if the name contains an 'a'
    - Checks if the name contains the string "abc"
    - Prints the second and third character of the string
    - Prints the last 2 characters of the string
     */
    public static void q8() {
        // This would go in public static void main(String[] args)
        Scanner scan = new Scanner(System.in);
        System.out.println("What is your name: ");
        String name = scan.nextLine();
        System.out.println(name.substring(0,1));    // name.charAt(0)
        System.out.println(name.toUpperCase());

        int i1 = name.indexOf('a');
        // We expect a boolean when we want to check if a name contains 'a'
        // indexOf -> returns the index of the given char/string/input. Returns -1 if the input is not part of the string
        boolean containsA = i1 != -1;

        int i2 = name.indexOf("abc");
        // Same as above
        boolean containsAbc = i2 != -1; // name.indexOf("abc") != -1;

        // new method called contains
        boolean containsAEasy = name.contains("a");

        System.out.println(name.substring(1,3));
        System.out.println(name.substring(name.length()-2));
    }

    // Describe the equals and compareTo methods
    public static void q9() {
        // compareTo gives a number as a result to show whether one string is greater than the other,
        // equals to gives true or false to if they are equal or not.

        // returns instead of gives
    }

    /*
    What is the output of the following method:
	public static void testMethod() {
		Integer x = 3;
		Integer y = new Integer(3);
		int z = 3;

		System.out.println(1 / 3);
		System.out.println((int) 3.999);
		System.out.println((double) 3);
		System.out.println((int) Math.random() * 5);
		System.out.println(1 == 3);
		System.out.println(x.equals(y));
		System.out.println(x.equals(z));
		System.out.println(y.equals(z));
	}
     */
    public static void q10() {
        Integer x = 3;
        Integer y = new Integer(3);
        int z = 3;

        System.out.println(1 / 3);
        System.out.println((int) 3.999);
        System.out.println((double) 3);
        System.out.println((int) Math.random() * 5);
        System.out.println(1 == 3);
        System.out.println(x.equals(y));
        System.out.println(x.equals(z));
        System.out.println(y.equals(z));

        /*
            0
            3
            3.0
            false
            true
            false
            false
         */
    }
}
