package arraylists;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class UsingArrayLists {

    public static void main(String[] args) {
        containsFive2(getIntegerArrayList());
        allNumbersAreEven(getIntegerArrayList());

//        reverseTheElementsInAnArrayList2();
    }

    public static void printAllElementsOfArrayList() {
        ArrayList<String> names = getStringArrayList();

        /*
            element1
            element2
            element3
            ....
         */

        for (int i = 0; i < names.size(); i++) {
            System.out.println(names.get(i));
        }

    }

    public static void printAllElementsOfArrayListInReverseOrder() {
        ArrayList<String> names = getStringArrayList();

        for (int i = names.size() - 1; i >= 0; i--) {
            System.out.println(names.get(i));
        }
    }

    public static void reverseTheElementsInAnArrayList1() {
        ArrayList<String> names = getStringArrayList();

        // create a new array list that has the values of names in reverse order
        ArrayList<String> namesReversed = new ArrayList<String>();
        for (int i = names.size() - 1; i >= 0; i--) {
            namesReversed.add(names.get(i));
        }

        System.out.println(names);
        System.out.println(namesReversed);
    }

    public static void addOneAfterEvens() {
        ArrayList<Integer> numbers = getIntegerArrayList();

        // add 1 after every occurrence of an even number
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) % 2 == 0) {  // the number is even
                numbers.add(i + 1, 1);
            }
        }

        System.out.println(numbers);
    }

    public static void addTwoAfterEvens() {
        ArrayList<Integer> numbers = getIntegerArrayList();

        // add 1 after every occurrence of an even number
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) % 2 == 0) {  // the number is even
                numbers.add(i + 1, 2);
                i++;
            }
        }

        System.out.println(numbers);
    }

    public static void removeEvens() {
        ArrayList<Integer> numbers = getIntegerArrayList();

        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) % 2 == 0) {  // even
                numbers.remove(i);
                i--;        // when you remove, always do an i--
            }
        }

        System.out.println(numbers);

    }

    public static void printStats() {
        ArrayList<Integer> numbers = getIntegerArrayList();

        int sum = 0;        // sum of all elements in numbers
        int min = numbers.get(0);        // smallest element in numbers
        int max = numbers.get(0);        // largest element in numbers
        int mean;       // arithmetic mean/average of all elements in numbers

        for (int i = 0; i < numbers.size(); i++) {
            int currentElement = numbers.get(i);

            sum = sum + currentElement;

            if (currentElement < min) {
                min = currentElement;
            }

            if (currentElement > max) {
                max = currentElement;
            }


        }

        mean = sum / numbers.size();

        System.out.println("Sum is: " + sum);
        System.out.println("Min is: " + min);
        System.out.println("Max is: " + max);
        System.out.println("Mean is: " + mean);
    }

    public static void containsFive1() {
        ArrayList<Integer> numbers = getIntegerArrayList();

        // print true if the array list contains 5 and false if it doesn't
        boolean foundFive = false;

        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) == 5) {
                foundFive = true;
            }
        }

        System.out.println(foundFive);
    }

    // HW1
    public static boolean containsFive2(ArrayList<Integer> numbers) {
        // return true if the array list contains 5 and false if it doesn't

        return false;
    }

    // HW2
    public static boolean allNumbersAreEven(ArrayList<Integer> numbers) {
        // return true if all elements in the array list are even and false if they aren't

        return false;
    }

    public static ArrayList<String> getStringArrayList() {
        ArrayList<String> names = new ArrayList<String>();
        names.add("Nik");
        names.add("Avidant");
        return names;
    }

    public static ArrayList<Integer> getIntegerArrayList() {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(7);
        numbers.add(8);
        numbers.add(12);
        numbers.add(8);
        numbers.add(9);
        numbers.add(5);
        return numbers;
        // [7, 8, 12, 8, 9]
    }
}
