package arraylists;

import java.util.ArrayList;

public class ArrayListMethods {

    public static void main(String[] args) {
        size();
    }

    public static void add() {
        ArrayList<String> names = new ArrayList<String>();
        System.out.println(names);

        names.add("Nik");
        System.out.println(names);

        names.add("Avi");
        System.out.println(names);

        names.add("Emma");
        names.add("Wilson");
        System.out.println(names);

        // add Scott after Avi. Emma and Wilson should come after Scott as expected

        // At what index will we want Scott? -> 2
        names.add(2, "Scott");
        System.out.println(names);

        // add Sheldon before Nik
        names.add(0, "Sheldon");
        System.out.println(names);

        // add Penny after Wilson
        names.add("Penny");
        System.out.println(names);

        // add Leonard at index 10 -> WILL THROW EXCEPTION
//        names.add(10, "Leonard");
//        System.out.println(names);
    }

    public static void get() {
        ArrayList<String> names = new ArrayList<String>();
        names.add("Sheldon");
        names.add("Nik");
        names.add("Avi");
        names.add("Scott");
        names.add("Emma");
        names.add("Wilson");
        names.add("Penny");
        System.out.println(names);

        // What is the first value?
        // Index -> 0
        System.out.println(names.get(0));

        // What is the last value?
        // Index -> 6
        System.out.println(names.get(6));

        // How do I get the 10th value?
        System.out.println(names.get(10));

    }

    public static void set() {
        ArrayList<String> names = new ArrayList<String>();
        names.add("Sheldon");
        names.add("Nik");
        names.add("Avi");
        names.add("Scott");
        names.add("Emma");
        names.add("Wilson");
        names.add("Penny");
        System.out.println(names);

        // instead of Avi, I want Avidant
        // Index -> 2
        names.set(2, "Avidant");        // replace the string at index 2
        System.out.println(names);

//        names.set(7, "Stuart");   -> WILL NOT WORK
//        System.out.println(names);
    }

    public static void remove() {
        ArrayList<String> names = new ArrayList<String>();
        names.add("Sheldon");
        names.add("Nik");
        names.add("Avi");
        names.add("Scott");
        names.add("Emma");
        names.add("Wilson");
        names.add("Penny");
        System.out.println(names);

        // remove Scott
        // Index -> 3
        names.remove(3);
        System.out.println(names);

        // remove Sheldon
        names.remove(0);
        System.out.println(names);

        // remove Penny
        names.remove(4);
        System.out.println(names);

//        names.remove(10);     -> WILL NOT WORK
    }

    public static void size() {
        ArrayList<String> names = new ArrayList<String>();
        System.out.println(names.size());
        names.add("Sheldon");
        System.out.println(names.size());
        names.add("Nik");
        System.out.println(names.size());
        names.add("Avi");
        System.out.println(names.size());
        names.add("Scott");
        System.out.println(names.size());
        names.add("Emma");
        System.out.println(names.size());
        names.add("Wilson");
        System.out.println(names.size());
        names.add("Penny");
        System.out.println(names.size());
        names.remove(0);
        System.out.println(names.size());


        /*
            String      -> variableName.length()
            Array       -> variableName.length
            ArrayList   -> variableName.size()
        */
    }

}
