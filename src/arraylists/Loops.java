package arraylists;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class Loops {

    public static void main(String[] args) {
        removeInFor();
    }

    public static void forLoop() {
//        String[] namesArray = new String[3];
//
//        for (int i = 0; i < namesArray.length; i++) {}

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(50);
        numbers.add(30);
        numbers.add(20);

        // write a for loop that prints each number of the array list on a new line
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }

        // print the sum of all values in numbers
        int total = 0;
        for (int i = 0; i < numbers.size(); i++) {
            total = total + numbers.get(i);
        }
        System.out.println(total);

//        // print the sum of all values in numbers
//        int sum = 0;
//        int counter = 0;
//        for (int i = 0; i < numbers.size(); i++) {
//            sum = sum + numbers.get(i);
//            counter = counter + 1;      // counter++
//        }
//        System.out.println(sum * 1.0 / counter);

        // print the sum of all values in numbers
        int sum = 0;
        for (int i = 0; i < numbers.size(); i++) {
            sum = sum + numbers.get(i);
        }
        System.out.println(sum * 1.0 / numbers.size());
    }

    // for-each loop is the same as an enhanced for loop
    public static void enhancedForLoop() {
//        String[] names = new String[3];
//
//        for (String name : names) {
//        }

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(50);
        numbers.add(30);
        numbers.add(20);

        // write a loop that prints each number of the array list on a new line
        for (Integer number : numbers) {
            System.out.println(number);
        }

        // print the sum of all values in numbers
        int sum = 0;
        for (Integer value : numbers) {
            sum += value;
        }
        System.out.println(sum);

        // print the sum of all values in numbers
        System.out.println(sum * 1. / numbers.size());

    }

    public static void removeUsingWhileLoop() {
        ArrayList<String> names = new ArrayList<String>();
        names.add("Sheldon");
        names.add("Nik");
        names.add("Avi");
        names.add("Scott");
        names.add("Emma");

        String name = "Nik";

        boolean found = false;  // true or false?
        int index = 0;
        while (index < names.size()) {
            if (name.equals(names.get(index))) {
                names.remove(index);
                found = true;   // true or false?
            } else {
                index++;
            }
        }
        System.out.println("Name was removed: " + found);
    }

    public static void removeInFor() {
        ArrayList<Integer> values = new ArrayList<Integer>();
        values.add(3);
        values.add(7);
        values.add(4);
        values.add(6);
        values.add(9);
        values.add(11);

        // remove all even numbers from values
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i) % 2 == 0) {
                values.remove(i);
                i--;
            }
        }

        System.out.println(values);
    }
}
