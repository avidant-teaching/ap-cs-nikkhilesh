package arraylists;

import java.util.ArrayList;

public class ArrayListReview {

    // [] -> arrays
    // <> -> type of ArrayList
    // (), {} -> everywhere else

    public static void main(String[] args) {
        // ArrayList: a data structure that holds data of the same type in a specific order

        // Create an ArrayList of type String with variable name cities
        // TypeYouAreCreating variableName = new TypeYouAreCreating();
        ArrayList<String> cities = new ArrayList<String>();

        // insert San Jose into cities
        cities.add("San Jose");

        // insert Atlanta into cities
        cities.add("Atanta");

        // insert Boston into cities before Atlanta
        cities.add(1, "Boston");    // 1 is the index where you want Boston to be after it's added

        // print the 3rd city from cities
        System.out.println(cities.get(2));

        // update the 2nd city in cities to New York
        cities.set(1, "New York");

        // print the number of cities in cities
        System.out.println(cities.size());

        // remove San Jose from cities
        cities.remove(0);

        // What is the index of New York?
        // 0
    }

}
