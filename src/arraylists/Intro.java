package arraylists;

import java.util.ArrayList;

public class Intro {

    public static void main(String[] args) {
        arrays();
        arrayLists();
    }

    public static void arrays() {
        String[] names = new String[3];
        names[0] = "Nik";
        names[1] = "Avi";
        names[2] = "Eclairs";
        // cannot change the size
    }

    public static void arrayLists() {
        // array vs array list -> array has a fixed size, array list doesn't
        // array is a consecutive space in memory
        // array list -> data can be all over the place

        String[] names = new String[3];
        int[] days = new int[4];

//        Class variableName;

        // this ArrayList has String
        ArrayList<String> students = new ArrayList<String>();
        ArrayList<Integer> numbers = new ArrayList<Integer>();

//        ArrayList -> anything (and this is bad!!!)
//        ArrayList<String> -> only hold String
    }

}
