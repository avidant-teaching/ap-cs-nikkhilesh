package repeatingarraylists;

import java.util.ArrayList;

public class UsingAnArrayList {


    public static void main(String[] args) {
        ArrayList<Integer> dates = new ArrayList<Integer>();
        dates.add(2);
        dates.add(22);
        dates.add(21);
        dates.add(19);
        dates.add(2);
        dates.add(12);
        dates.add(22);
        dates.add(24);
        System.out.println(dates);
//        System.out.println(reverse(dates));
        reverseInPlace(dates);
        System.out.println(dates);
    }

    public static void addSevenAfterTheFirstOdd(ArrayList<Integer> dates) {
//        for (int i = 0; i < dates.size(); i++) {
//            if (dates.get(i) % 2 == 1) {
//                dates.add(i + 1, 7);
//                i = dates.size();   // or any number that ends the loop
//            }
//        }

//        boolean added = false;
//        for (int i = 0; i < dates.size() && !added; i++) {
//            if (dates.get(i) % 2 == 1) {
//                dates.add(i + 1, 7);
//                i++;
//                added = true;
//            }
//        }

        boolean added = false;
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 1 && !added) {
                dates.add(i + 1, 7);
                i++;
                added = true;
            }
        }

        System.out.println(dates);
    }

    public static void removeTheSecondOdd(ArrayList<Integer> dates) {
        int oddCount = 0;
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 1) {
                oddCount++;
                if (oddCount == 2) {
                    dates.remove(i);
                    i--;
                }
            }
        }
    }

    public static void printTheMinAndMax(ArrayList<Integer> dates) {
//        int min = 0;
//        int max = 0;
        // Setting min and max to 0 can make a mistake. In this example it prints "MIN is: 0" since all the elements
        // in the list are larger than 0

        // Option 1: set min and max to dates.get(0)
        // Option 2: min -> Integer.MAX_VALUE and max -> Integer.MIN_VALUE

        int min = dates.get(0);
        int max = dates.get(0);

        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) > max) {
                max = dates.get(i);
            }
            if (dates.get(i) < min) {
                min = dates.get(i);
            }
        }

        System.out.println("MIN is: " + min);
        System.out.println("MAX is: " + max);
    }

    public static void printTheIndexOfTheFirstOdd(ArrayList<Integer> dates) {
        boolean stillNeedToPrint = true;
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 1 && stillNeedToPrint) {
                System.out.println(i);
                stillNeedToPrint = false;
            }
        }
    }

    public static void isThereAnEvenNumber(ArrayList<Integer> dates) {
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 0) {
                System.out.println("There is an even number");
            }
        }

        // If there is an even number say "found an even" else say "no even found" (only 1 statement should be printed)
        boolean foundEven = false;
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 0) {
                foundEven = true;
            }
        }
        if (foundEven) {
            System.out.println("found an even");
        } else {
            System.out.println("no even found");
        }
    }

    public static boolean hasEven(ArrayList<Integer> dates) {
         for (int i = 0; i < dates.size(); i++) {
             if (dates.get(i) % 2 == 0) {
                 return true;
             }
         }
         return false;
    }

    // HW1
    public static void areAllEvens(ArrayList<Integer> dates) {
        // if all elements are even, print a single statement that says "all elements are even"
        // else print a single statement that says "not all elements are even"

//        for (int i = 0; i < dates.size(); i++) {
//            if (dates.get(i) % 2 == 0) {
//                i++;
//            }
//            if (i == dates.size() - 1) {
//                System.out.println("all elements are even");
//            }
//        }
//        System.out.println("not all elements are even");

        int even = 0;
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 0) {
                even++;
            }
        }
        if (even == dates.size()) {
            System.out.println("all elements are even");
        } else {
            System.out.println("not all elements are even");
        }
    }

    // HW2
    public static boolean hasOnlyEvens(ArrayList<Integer> dates) {
        // return true if all elements are even. Else return false

//        for (int i = 0; i < dates.size(); i++) {
//            if (dates.get(i) % 2 == 0) {
//                i++;
//            }
//            if (i == dates.size() - 1) {
//                return true;
//            }
//        }
//        return false;

        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 1) {
                return false;
            }
        }
        return true;
    }

    public static int returnLastOdd(ArrayList<Integer> dates) {
        // return the last odd element in the list. If there are no odds, return 0

//        // [123, 123, 123, 123, 123]
//        // What we want: 0, 1, 2, 3, 4 OR 4 3, 2, 1, 0
//        // What we'll get: 5, 4, 3, 2, 1
//        for (int i = dates.size(); i > 0; i--) { }

        for (int i = dates.size() - 1; i >= 0; i--) {
            if (dates.get(i) % 2 == 1) {
                return dates.get(i);
            }
        }
        return 0;
    }

    public static void allElementPairsHaveTheSameParity(ArrayList<Integer> dates) {
        // [1, 3, 4, 6, 5, 6]
        // parity = odd/even
        // Either print "all pairs have the same parity" or "all pairs do not have the same parity"
        // you can assume dates has an even number of elements

        // how to cleanup the if condition
//        for (int i = 0; i < dates.size(); i++) {
//            if ((dates.get(i) % 2 == 0 && dates.get(i + 1) % 2 == 0) ||
//                    (dates.get(i) % 2 == 1 && dates.get(i + 1) % 2 == 1)) {
//
//                // transitive property -> if a == b and b == c, then a == c
//
//                // a == b and c == b -> a = c
//                // dates.get(i) % 2 == 0 && dates.get(i + 1) % 2 == 0 -> dates.get(i) % 2 == dates.get(i + 1) % 2
//
//            }
//        }

        // standard pattern for "do all elements meet a specific condition"
//        int countOfTimesTheConditionWasMet = 0;
//        for (int i to represent every index in the arraylist) {
//            if (condition is being met) {
//                countOfTimesTheConditionWasMet++;
//            }
//        (if checking for pairs, count pairs count here and check against the pair count instead of list size)
//        }
//        if (countOfTimesTheConditionWasMet == list.size()) {
//            System.out.println("condition was always met");
//        } else {
//            System.out.println("condition was not always met");
//        }


        // how to check pairs in a list

        // 2 ways to look at pairs in a list: [1, 3, 4, 6, 5, 6]
        // 1 way: (1, 3), (4, 6), (5, 6)
        // another way: (1, 3), (3, 4), (4, 6), (6, 5), (5, 6)

        // generally questions will explicitly tell you. But if not,
        // if there is a guarantee of even number of elements, use way 1
        // if not, use method 2

        // Using method 1: [1, 3, 4, 6, 5, 6] -> (1, 3), (4, 6), (5, 6)
        // instead of i++, you do i += 2
        // condition -> all pairs have the same parity
        int parityCounter = 0;
        int pairCounter = 0;
        for (int i = 0; i < dates.size(); i += 2) {
            if (dates.get(i) % 2 == dates.get(i + 1) % 2) {     // elements at index i and i + 1 are either both odd or both even
                parityCounter++;
            }
            pairCounter++;
        }
        if (parityCounter == pairCounter) {
            System.out.println("all pairs have the same parity");
        } else {
            System.out.println("all pairs do not have the same parity");
        }


        // Using method 2: [1, 3, 4, 6, 5, 6] -> (1, 3), (3, 4), (4, 6), (6, 5), (5, 6)
        // instead of i < dates.size(), you do i < dates.size() - 1
        // condition -> all pairs have the same parity
        parityCounter = 0;
        pairCounter = 0;
        for (int i = 0; i < dates.size() - 1; i++) {
            if (dates.get(i) % 2 == dates.get(i + 1) % 2) {     // elements at index i and i + 1 are either both odd or both even
                parityCounter++;
            }
            pairCounter++;
        }
        if (parityCounter == pairCounter) {
            System.out.println("all pairs have the same parity");
        } else {
            System.out.println("all pairs do not have the same parity");
        }

    }

    public static void howManyOdds(ArrayList<Integer> dates) {
        int odds = 0;
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 1) {
                odds++;
            }
        }
        System.out.println("there are " + odds + " odd numbers");
    }

    public static ArrayList<Integer> reverse(ArrayList<Integer> dates) {
        // return a new ArrayList with the elements of dates. Do not change dates

        // create a new ArrayList called reversed
        // make a loop (from back to front)
        // inside the loop, add to reversed
        // return reversed

        ArrayList<Integer> reversed = new ArrayList<Integer>();
        for (int i = dates.size() - 1; i >= 0; i--) {
            reversed.add(dates.get(i));
        }
        return reversed;
    }

    // HW1 (it's okay if you don't get it. Think of it as extra credit)
    public static void reverseInPlace(ArrayList<Integer> dates) {
        // reverse the elements of dates without creating a new ArrayList

        /*
            [0, 1, 2, 3, 4]
            [4, 1, 2, 3, 0] // swap the first and last elements
            [4, 3, 2, 1, 0] // swap the second and second from last element element

         */

        // assume 10 element list
        // step 0 -> swapped index 0 and index 9
        // step 1 -> swapped index 1 and index 8
        // step 2 -> index 2, 7
        // step 3 -> index 3, 6
        // step 4 -> index 4, 5

        // one value goes up by one, another goes down by 1

        // 0, 9 -> 0 + 9 = 9
        // 1, 8 -> 1 + 8 = 9
        // 2, 7 -> 2 + 7 = 9
        // 3, 6 -> 3 + 6 = 9

        // if you know the first index and the sum of indices, you know the second index
        // sum of indices -> size - 1

        for (int i = 0; i < dates.size() / 2; i++) {
//        for (int i = 0; i < dates.size() - 1 - i / 2; i++) { // keep going while index1 < index2
                int index1 = i;
                int index2 = dates.size() - 1 - i;

                int value1 = dates.get(index1);
                int value2 = dates.get(index2);

                dates.set(index1, value2);
                dates.set(index2, value1);
        }

        System.out.println(dates);
    }


}
