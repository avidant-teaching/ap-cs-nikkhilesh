package repeatingarraylists;

public class SortingAnArrayList {

    // selection sort
        // you go over the list, find the smallest element at some index k
        // swap the element at index k with index 0
        // go over the list from index 1, find the smallest element at some index k
        // swap the element at index k with index 1
        // ...
        // go over the list from index size() - 2 (last 2 elements), find the smallest element at some index k
        // swap the element at index size-2 with element at index k

    /*

    Element 0:
    [5, 4, 67, 3, 756, 44, 3234]
    1. smallest element -> 3 at index 3
    2. swap elements at index 0 and 3

    Element 1:
    [3, 4, 67, 5, 756, 44, 3234]
    1. smallest element from index 1 till the end -> 4 at index 1
    2. swap elements at index 1 and index 1

    Element 2:
    [3, 4, 67, 5, 756, 44, 3234]
    1. Smallest element from index 2 till the end -> 5 at index 3
    2. Swap index 2 and 3

    [3, 4, 5, 67, 756, 44, 3234]

     */



    // insertion sort
        // used when you are creating a new sorted list
        // you have a list that is already sorted (or is empty)
        // you need to add a new element, so you search for the ideal index
        // push all the elements after the ideal index to the right (add an empty spot where we want our element)
        // add the element to the "ideal" index

    // merge sort (Will look at it in Unit 10)

    // HW1:
        // for selection sort and insertion sort, how many elements would we need to look at if the size of the array is 10
}
