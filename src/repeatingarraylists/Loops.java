package repeatingarraylists;

import java.util.ArrayList;

public class Loops {

    public static void main(String[] args) {
        ArrayList<Integer> dates = new ArrayList<Integer>();
        dates.add(2);
        dates.add(23);
        dates.add(2);
        dates.add(12);
        dates.add(22);
        whileLoops(dates);
    }

    public static void basicForLoop(ArrayList<Integer> dates) {
        // for each element of dates, print true if it's even, false if it's odd
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 0) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }
    }

    public static void basicReverseForLoop(ArrayList<Integer> dates) {
        // for each element of dates (in reverse order), print true if it's even, false if it's odd
        for (int i = dates.size() - 1; i >= 0; i--) {
            if (dates.get(i) % 2 == 0) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }
    }

    // enhanced for loop: aka for-each loop
    public static void enhancedForLoop(ArrayList<Integer> dates) {
        // for each element of dates, print true if it's even, false if it's odd
        for (Integer date : dates) {
            if (date % 2 == 0) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }
    }

    public static void whileLoops(ArrayList<Integer> dates) {
        // for each element of dates, print true if it's even, false if it's odd

        // initialization
        int i = 0;

        // verification/check/stop
        while(i < dates.size()) {

            if (dates.get(i) % 2 == 0) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }

            // Update/jump
            i++;        // i = i + 1 OR i += 1
        }
    }



}
