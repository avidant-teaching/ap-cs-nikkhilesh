package repeatingarraylists;

import java.util.ArrayList;

public class Intro {

    public static void main(String[] args) {
        arrayLists();
    }

    public static void strings() {

        // Create a string with name "month" and store the current month in it
        String month1 = "January";
        String month2 = new String("January");

        // Type variableName = new Type(???);


    }

    public static void arrays() {
        // array -> a sequence of memory to store a specific number of items of a specific type

        // Create an array of String with size 5
        // Type -> String[]
        // variableName -> numbers
        String[] numbers = new String[5];

        // Set the 3rd element of the array to "Nik"
        numbers[2] = "Nik";

        // print the 3rd element of the array
        System.out.println(numbers[2]);
    }

    public static void arrayLists() {
        // arrayList -> data structure that can store any number of items of a specific type

        // Create an Arraylist with type Integer and name numbers
        // Type -> ArrayList<Integer>
        // variableName -> numbers
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        // add -> end
        // add -> at a specific index
        // 1. Add the number 5 to the end
        // 2. Add the number 3 to the end
        // 3. Add the number 1 at index 0
        // 4. Add the number 4 so it is after 3
        // 5. Add the number 2 so it is after 1

        numbers.add(5);
        numbers.add(3);
        numbers.add(0, 1);
        numbers.add(4);
        numbers.add(1, 2);

        System.out.println(numbers);

        // remove -> remove from a specific index
        // remove the last element

        numbers.remove(4);

        System.out.println(numbers);

        // set (update/replace) -> at an index
        // replace the number 1 with the number 4

        numbers.set(0, 4);

        System.out.println(numbers);

        // get -> from the index
        // print the second element
        // print the last element

        System.out.println(numbers.get(1));
        System.out.println(numbers.get(3));

        // size -> number of elements
        // print the size of numbers

        System.out.println(numbers.size());


    }
}
