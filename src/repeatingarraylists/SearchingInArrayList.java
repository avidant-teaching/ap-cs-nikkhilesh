package repeatingarraylists;

import java.util.ArrayList;

public class SearchingInArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> dates = new ArrayList<Integer>();
        dates.add(2);
        dates.add(22);
        dates.add(21);
        dates.add(19);
        dates.add(2);
        dates.add(12);
        dates.add(22);
        dates.add(24);
        System.out.println(dates);
//        System.out.println(reverse(dates));
//        reverseInPlace(dates);
        System.out.println(dates);
    }

    // data -> sorted or unsorted
    // sorted -> [1, 3, 5, 6, 1100]
    // unsorted -> [6, 3, 7, 5, 8, 100]

    // sequential search -> only way to search in unsorted data
    public static int sequentialSearch(ArrayList<Integer> dates, int number) {
        // return the first index that matches number. If number doesn't exist in the arraylist, return -1

        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) == number) {
                return i;
            }
        }
        return -1;

        // if the list size is 10, we'd have to look at 10 elements
        // if the list size is 100, we'd have to look at 100 elements
        // if the list size is 10000, we'd have to look at 10000 elements
        // if the list size is 100000000, we'd have to look at 100000000 elements
    }

    // binary search -> best way to search sorted data
    public static void binarySearch(ArrayList<Integer> dates, int number) {
        // dates -> [1, 3, 4, 5, 6, 100, 1100]
        // value -> 75

        // step 1 -> find the middle element (index -> 3, value -> 5)
        // is 75 > 5 or 75 < 5 or is 75 = 5
        // since 75 > 5, we now ignore elements 0, 1, 2, 3. Now look at sublist [6, 100, 1100]
        // step 2 find the middle element in the sublist (index -> 5, value -> 100)
        // is 75 > 100, 75 < 100, 75 = 100
        // since 75 < 100, we can ignore the elements 5, 6. Now look at the sublist [6]
        // Step 3 -> since there's only 1 element, is 6 == 75? No. So 75 is not in the list :)

        // 6 elements -> 3 steps
    }

    // if you don't know if the data is sorted or not, use sequential search

    // if you don't know whether the data is sorted or not, *do not first check if the data is sorted and then do a binary search*
    // The reason is that the check on if the data is sorted is the same as a sequential search so it'll be faster to just do a sequential search
}
