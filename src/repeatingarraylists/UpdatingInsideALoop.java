package repeatingarraylists;

import java.util.ArrayList;

public class UpdatingInsideALoop {

    public static void main(String[] args) {
        ArrayList<Integer> dates = new ArrayList<Integer>();
        dates.add(2);
        dates.add(23);
        dates.add(21);
        dates.add(19);
        dates.add(2);
        dates.add(12);
        dates.add(22);
        removingInAForLoop(dates);
    }

    public static void addingOrRemovingInsideAForEachLoop() {
        // YOU CANNOT ADD OR REMOVE INSIDE A FOR-EACH LOOP
        // Will throw a ConcurrentModificationException
    }

    public static void addingInAForLoop(ArrayList<Integer> dates) {
        // for each element, if the number is odd, add 1 after it

        // HW:
        // 1. Why does this not work?: You add a new value, it increases the size of the list, you keep doing that
        // 2. What are the 2 things wrong with the logic?
            // 1. Adds a new value and don't do an extra i++
            // 2. 1 is added at the index of the odd number (which makes it go before), should happen at i + 1
        // 3. Fix the code so it works
//        for (int i = 0; i < dates.size(); i++) {
//            if (dates.get(i) % 2 == 1) {
//                dates.add(i, 1);
//            }
//        }

        // Fixed code:
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 1) {
                dates.add(i + 1, 1);
                i++;
            }
        }

        System.out.println(dates);
    }

    private static void removingInAForLoop(ArrayList<Integer> dates) {
        // Remove the odd numbers from dates

//        System.out.println(dates);
//        for (int i = 0; i < dates.size(); i++) {
//            if (dates.get(i) % 2 == 1) {
//                dates.remove(i);
//                i++;
//            }
//        }
//        System.out.println(dates);

        // Let's trace
        /*
            [2, 23, 21, 19, 2, 12, 22]
            dates.size() -> 7
            i = 0, i < dates.size()
                2 is not odd
            i++
            i = 1, i < dates.size()
                23 is odd
                dates.remove(1)
                [2, 21, 19, 2, 12, 22]
                i++
            i++
            i = 3 (we reached element 2 and skipped 21 and 19)

            Get rid of the i++ inside the if

         */

//        System.out.println(dates);
//        for (int i = 0; i < dates.size(); i++) {
//            if (dates.get(i) % 2 == 1) {
//                dates.remove(i);
//            }
//        }
//        System.out.println(dates);

        // Let's trace
        /*
            [2, 23, 21, 19, 2, 12, 22]

                i       dates.size()    i < dates.size()        element     do you do anything

                0           7               true                    2        no
                i++
                1           7               true                    23      dates.remove(1)
                [2, 21, 19, 2, 12, 22]
                i++
                2           6               true                    19      dates.remove(2)     [ we skipped 21 ]
                [2, 21, 2, 12, 22]
                3           5               true                    12      no                  [ we skipped 2 ]
         */

        System.out.println(dates);
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i) % 2 == 1) {
                dates.remove(i);
                i--;
            }
        }
        System.out.println(dates);
    }
}
